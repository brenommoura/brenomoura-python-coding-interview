from datetime import datetime
from math import floor

from rest_framework.test import APITestCase, APIClient

from app.users.models import User


class TestUsers(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.client = APIClient()
        cls.user_1 = User.objects.create(
            name='User Name 1',
            last_name='User Last Name 1',
            email='test@test111.com',
            birth_date='2000-01-01'
        )
        cls.user_2 = User.objects.create(
            name='User Name 2',
            last_name='User Last Name 2',
            email='test@test222.com',
            birth_date='2000-01-01'
        )
        cls.users = [cls.user_1, cls.user_2]

    @staticmethod
    def build_user(user: User):
        return {
            'id': user.id,
            'name': user.name,
            'email': user.email,
            'last_name': user.last_name,
            'age': floor((datetime.now() - datetime.strptime(user.birth_date, '%Y-%m-%d')).days / 365),
            'is_active': user.is_active,
            'is_staff': user.is_staff,
            'is_superuser': user.is_superuser,
        }

    def test_users(self):
        response = self.client.get('/user/users')
        self.assertEqual(response.status_code, 200)
        expected_users = [self.build_user(user) for user in self.users]
        self.assertListEqual(response.json(), expected_users)
