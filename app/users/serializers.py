from rest_framework import serializers

from app.users.models import User


class UserSerializer(serializers.ModelSerializer):
    age = serializers.IntegerField()

    class Meta:
        model = User
        fields = ['id', 'age', 'name', 'last_name', 'email', 'is_staff', 'is_active', 'is_superuser']
