from django.urls import path
from .views import ListProducts, UpdateProduct

app_name = 'store'

urlpatterns = [
    path('products', ListProducts.as_view()),
    path('products/<int:pk>', UpdateProduct.as_view())
]
