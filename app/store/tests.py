from rest_framework.test import APITestCase, APIClient

from app.store.models import ProductCategory, Products


class TestProducts(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.client = APIClient()
        cls.category = ProductCategory.objects.create(
            name='category1'
        )
        cls.product = Products.objects.create(
            name='Product 1',
            is_active=False,
        )
        cls.product.category.add(cls.category)

    def test_create_product(self):
        data = {
            'name': 'Product 2',
            'is_active': True,
            'category': [self.category.id]
        }
        response = self.client.post('/store/products', data=data)
        self.assertEqual(response.status_code, 201)
        products = Products.objects.filter(name=data['name'])
        self.assertTrue(products.exists())

    def test_update_product(self):
        data = {
            'is_active': True,
        }
        response = self.client.patch(f'/store/products/{self.product.id}', data=data)
        self.assertEqual(response.status_code, 200)
        self.product.refresh_from_db()
        self.assertEqual(self.product.is_active, data['is_active'])

