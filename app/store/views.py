from rest_framework.generics import CreateAPIView, UpdateAPIView

from app.store.models import Products
from app.store.serializers import ProductSerializer


class ListProducts(CreateAPIView):
    serializer_class = ProductSerializer
    queryset = Products.objects.all()


class UpdateProduct(UpdateAPIView):
    serializer_class = ProductSerializer
    queryset = Products.objects.all()
